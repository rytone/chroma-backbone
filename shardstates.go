package main

type ShardState int32

const (
	ShardOffline ShardState = iota
	ShardConnecting
	ShardOnline
	ShardUnresponsive
)
