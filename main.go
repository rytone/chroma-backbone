package main

import (
	"net/http"

	"github.com/golang/protobuf/proto"
	"github.com/gorilla/websocket"
	"github.com/sirupsen/logrus"

	cproto "gitlab.com/rytone/chroma-proto/golang"
)

func (s *ShardManager) ServeShard(c *websocket.Conn) {
	for {
		_, msg, err := c.ReadMessage()
		if err != nil {
			logrus.Error(err)
			return
		}

		baseMsg := cproto.Message{}
		err = proto.Unmarshal(msg, &baseMsg)
		if err != nil {
			logrus.Error(err)
			return
		}

		switch baseMsg.Type {
		default:
			logrus.Errorf("encountered unexpected message type %d", baseMsg.Type)
			return
		case 0: // shard initialize
			err = s.HandleShardInitialize(baseMsg.Payload, c)
			if err != nil {
				logrus.Error(err)
				return
			}
		}
	}
}

// remove origin workaround in prod
var upgrader = websocket.Upgrader{CheckOrigin: func(r *http.Request) bool { return true }}

func (s *ShardManager) UpgradeShard(w http.ResponseWriter, r *http.Request) {
	logrus.Info("upgrading and serving new shard")
	c, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		logrus.Error(err)
		return
	}
	defer c.Close()
	s.ServeShard(c)
}

func main() {
	logrus.SetLevel(logrus.DebugLevel)
	logrus.Info("starting")

	manager := ShardManager{1, 1, 2500, 1000, map[string]Shard{}}

	http.HandleFunc("/shard", manager.UpgradeShard)
	logrus.Fatal(http.ListenAndServe("10.0.0.15:62831", nil))
}
