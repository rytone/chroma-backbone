package main

import (
	"github.com/golang/protobuf/proto"
	"github.com/gorilla/websocket"
	"github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"

	protoc "gitlab.com/rytone/chroma-proto/golang"
)

func (s *ShardManager) HandleShardInitialize(payload []byte, c *websocket.Conn) error {
	msg := protoc.ShardInit{}
	err := proto.Unmarshal(payload, &msg)
	if err != nil {
		return err
	}
	logrus.Infof("shard is version %s", msg.Version)
	// TODO check version for incompatibilities

	shard := Shard{}
	shard.DiscordId = 0 // replace with real id based on demand
	shard.State = ShardConnecting
	shard.Connection = c

	uuid := uuid.NewV4().String()

	s.Shards[uuid] = shard

	response := &protoc.BackboneResponse{shard.DiscordId, s.TotalShards, uuid}
	resser, err := proto.Marshal(response)
	if err != nil {
		return err
	}

	reswrap := &protoc.Message{1, resser}
	finalser, err := proto.Marshal(reswrap)
	if err != nil {
		return err
	}

	err = c.WriteMessage(websocket.BinaryMessage, finalser)
	if err != nil {
		return err
	}

	logrus.Debug("responded to shard init")

	return nil
}
