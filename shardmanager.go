package main

import "github.com/gorilla/websocket"

type Shard struct {
	DiscordId  int32
	State      ShardState
	Connection *websocket.Conn
}

type ShardManager struct {
	NeededShards       int32
	TotalShards        int32
	HeartbeatRate      int32
	HeartbeatTolerance int32
	Shards             map[string]Shard
}
