package main

import (
	"os"
	"os/signal"

	"github.com/golang/protobuf/proto"
	"github.com/gorilla/websocket"
	"github.com/sirupsen/logrus"

	cproto "gitlab.com/rytone/chroma-proto/golang"
)

func closeConnection(c *websocket.Conn) {
	err := c.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
	if err != nil {
		logrus.Error(err)
		return
	}
	c.Close()

}

func main() {
	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt)

	logrus.Info("connecting to ws://10.0.0.15:62831/shard")

	c, _, err := websocket.DefaultDialer.Dial("ws://10.0.0.15:62831/shard", nil)
	if err != nil {
		logrus.Fatal(err)
	}
	defer c.Close()

	go func() {
		for {
			_, p, err := c.ReadMessage()
			if err != nil {
				return
			}

			basemsg := cproto.Message{}
			err = proto.Unmarshal(p, &basemsg)
			if err != nil {
				closeConnection(c)
				logrus.Fatal(err)
			}
			switch basemsg.Type {
			case 1: // backbone response
				bbres := cproto.BackboneResponse{}
				err = proto.Unmarshal(basemsg.Payload, &bbres)
				if err != nil {
					closeConnection(c)
					logrus.Fatal(err)
				}
				logrus.Infof("READY.\nid: %s\nshard id: %d\ntotal shards: %d", bbres.BackboneId, bbres.ShardId, bbres.ShardCount)
			}
		}
	}()

	logrus.Info("sending shard connect/verify")

	shardconn := &cproto.ShardInit{"v1"}
	connSer, err := proto.Marshal(shardconn)
	if err != nil {
		closeConnection(c)
		logrus.Fatal(err)
	}

	shardwrap := &cproto.Message{0, connSer}
	msgSer, err := proto.Marshal(shardwrap)
	if err != nil {
		closeConnection(c)
		logrus.Fatal(err)
	}

	err = c.WriteMessage(websocket.BinaryMessage, msgSer)
	if err != nil {
		closeConnection(c)
		logrus.Fatal(err)
	}

	for {
		select {
		case <-interrupt:
			logrus.Info("got interrupt, closing down!")
			closeConnection(c)
			return
		}
	}
}
